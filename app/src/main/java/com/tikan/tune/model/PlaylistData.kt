package com.tikan.tune.model

data class PlaylistData(val id: String, var songName: String, var path: String)
