package com.tikan.tune.service

import android.annotation.SuppressLint
import android.app.*
import android.content.Context
import android.content.Intent
import android.media.session.MediaSession
import android.os.Build
import android.os.IBinder
import androidx.core.app.NotificationCompat
import com.tikan.tune.R
import com.tikan.tune.view.activity.MainActivity

class MusicForegroundService: Service() {

    @SuppressLint("RestrictedApi")
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        createNotificationChannel()

        val notificationIntent = Intent(this,MainActivity::class.java)

        val pendingIntent = PendingIntent.getActivity(this,0,notificationIntent,0)

        val notification = Notification.Builder(this,"channel_id")
            .setStyle(Notification.MediaStyle())
            .addAction(Notification.Action.Builder(R.drawable.ic_play,"pause",pendingIntent).build())
            .setContentTitle("This is title")
            .setContentText("this is text")
            .setSmallIcon(R.drawable.ic_pause)
            .setContentIntent(pendingIntent)
            .setTicker("this is ticker text")
            .build()

        startForeground(1,notification)
        return super.onStartCommand(intent, flags, startId)
    }

   private fun createNotificationChannel(){

       if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
           val name = "my_channel"
           val descriptionText = "This is channel description"
           val importance = NotificationManager.IMPORTANCE_DEFAULT
           val channel = NotificationChannel("channel_id",name,importance).apply {
               description = descriptionText
           }
           val notificationManager : NotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
           notificationManager.createNotificationChannel(channel)
       }
    }
    override fun onBind(intent: Intent?): IBinder? {
        TODO("Not yet implemented")
    }
}