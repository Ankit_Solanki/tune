package com.tikan.tune.player

import android.media.MediaPlayer
import com.tikan.tune.model.PlaylistData

object MPlayer {

    private val player = MediaPlayer()
    var playList: ArrayList<PlaylistData> = arrayListOf()
    var currentSongData:PlaylistData? = null

    fun onComplete(onComplete:()->Unit){
        player.setOnCompletionListener {
            onComplete()
        }
    }

    fun play(){
        player.start()
    }

    fun play(path:String?){
        player.reset()
        player.setDataSource(path)
        player.prepare()
        player.start()
    }

    fun pause(){
        player.pause()
    }

    fun reset(){
        player.reset()
    }

    fun stop(){
        player.stop()
    }

    fun release(){
        player.release()
    }

    fun next(){
        val currentIndex = playList.indexOfFirst {
            it.path == currentSongData?.path
        }
        val nextPath = playList.getOrNull(currentIndex+1)

        if (nextPath != null){
            play(nextPath.path)
            currentSongData?.path = nextPath.path
            currentSongData?.songName = nextPath.songName
        }
    }

    fun previous(){

        val previousIndex = playList.indexOfFirst {
            it.path == currentSongData?.path
        }
        val previousPath = playList.getOrNull(previousIndex-1)

        if (previousPath != null){
            play(previousPath.path)
            currentSongData?.path = previousPath.path
            currentSongData?.songName = previousPath.songName
        }

    }
}