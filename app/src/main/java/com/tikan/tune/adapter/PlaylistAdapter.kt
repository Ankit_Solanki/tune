package com.tikan.tune.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tikan.tune.databinding.ItemPlaylistBinding
import com.tikan.tune.model.PlaylistData

class PlaylistAdapter(val list: List<PlaylistData>, val listener: OnSongClickListener?) : RecyclerView.Adapter<PlaylistAdapter.VH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val binding =
            ItemPlaylistBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return VH(binding)
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(list[position])
    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class VH(private val binding: ItemPlaylistBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(playlistItem: PlaylistData) {
            binding.tvId.text = playlistItem.id
            binding.tvSong.text = playlistItem.songName

            itemView.setOnClickListener {
                listener?.onSongClick(playlistItem)
            }
        }
    }

    interface OnSongClickListener{
        fun onSongClick(playlistItem:PlaylistData)
    }
}