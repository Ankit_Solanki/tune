package com.tikan.tune.view.fragment

import android.media.MediaMetadataRetriever
import android.media.MediaPlayer
import android.os.Bundle
import android.os.FileUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.URLUtil
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.tikan.tune.R
import com.tikan.tune.adapter.PlaylistAdapter
import com.tikan.tune.databinding.FragmentPlayingMusicBinding
import com.tikan.tune.model.PlaylistData
import com.tikan.tune.player.MPlayer

class PlayingMusicFragment : Fragment(R.layout.fragment_playing_music) {

    lateinit var binding: FragmentPlayingMusicBinding
    lateinit var playlist: List<String>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentPlayingMusicBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        MPlayer.currentSongData = arguments.getString("Music")

        MPlayer.play(MPlayer.currentSongData?.path)

        binding.tvSongName.text = MPlayer.currentSongData?.songName

        binding.ivPause.setOnClickListener {
            MPlayer.pause()
            binding.ivPlay.visibility = View.VISIBLE
            binding.ivPause.visibility = View.INVISIBLE
        }

        binding.ivPlay.setOnClickListener {
            MPlayer.play()
            binding.ivPause.visibility = View.VISIBLE
            binding.ivPlay.visibility = View.INVISIBLE
        }

        binding.ivNext.setOnClickListener {
            MPlayer.next()
            binding.tvSongName.text = MPlayer.currentSongData?.songName
            binding.ivPause.visibility = View.VISIBLE
            binding.ivPlay.visibility = View.INVISIBLE
        }

        binding.ivPrevious.setOnClickListener {
            MPlayer.previous()
            binding.tvSongName.text = MPlayer.currentSongData?.songName
            binding.ivPause.visibility = View.VISIBLE
            binding.ivPlay.visibility = View.INVISIBLE
        }

        binding.tvInitialTime.text = getDuration().toString()

        MPlayer.onComplete {
            binding.ivPause.visibility = View.INVISIBLE
            binding.ivPlay.visibility = View.VISIBLE
        }
    }

    fun getDuration(): Int {

        val mediaMetadataRetriever = MediaMetadataRetriever()

        mediaMetadataRetriever.setDataSource(MPlayer.currentSongData?.path)

        val durationStr =
            mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)

        val milliSecond = durationStr?.toInt()
        val minutes = (milliSecond?.rem((1000 * 60 * 60)))?.div((1000 * 60))

        return minutes!!
    }
}