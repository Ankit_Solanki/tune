package com.tikan.tune.view.fragment

import android.content.Intent
import android.database.Cursor
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat.startForegroundService
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.tikan.tune.R
import com.tikan.tune.adapter.PlaylistAdapter
import com.tikan.tune.databinding.FragmentPlaylistBinding
import com.tikan.tune.model.PlaylistData
import com.tikan.tune.player.MPlayer
import com.tikan.tune.service.MusicForegroundService

class PlaylistFragment:Fragment(R.layout.fragment_playlist) {

    lateinit var binding: FragmentPlaylistBinding
    lateinit var mediaPlayer: MediaPlayer
    lateinit var uri: Uri
    lateinit var projection: Array<String?>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentPlaylistBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.rvPlaylist.layoutManager =
            LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)

        val songList = getAllAudioFromDevice()
        MPlayer.playList = songList

        val adapter = PlaylistAdapter(songList,object : PlaylistAdapter.OnSongClickListener{
            override fun onSongClick(playlistItem: PlaylistData) {

                MPlayer.currentSongData = playlistItem
                val bundle = Bundle()
                bundle.putString("Music", playlistItem.toString())
                findNavController().navigate(R.id.action_playlistFragment_to_playingMusicFragment,bundle)
                val serviceIntent = Intent(requireContext(),MusicForegroundService::class.java)
                startForegroundService(requireContext(),serviceIntent)
            }
        })
        binding.rvPlaylist.adapter = adapter
    }


    private fun getAllAudioFromDevice(): ArrayList<PlaylistData> {

        val tempAudioList = ArrayList<PlaylistData>()

        uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
        projection =
            arrayOf(MediaStore.Audio.Media.DATA, MediaStore.Audio.Media.TITLE, MediaStore.Audio.Media._ID)
        val c: Cursor? =
            requireContext().contentResolver.query(uri, projection, null, null, null, null)

        if (c != null && c.count > 0) {

            while (c.moveToNext()) {

                val path: String = c.getString(0)
                val songName: String = c.getString(1)
                val id: String = c.getString(2)
                val name = path.substring(path.lastIndexOf("/")+1)
                val playlistData: PlaylistData = PlaylistData(id, songName, path)

                Log.d("TAG", name)
                tempAudioList.add(playlistData)
            }
            c.close()
        }
        return tempAudioList

    }

}